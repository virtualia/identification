﻿<%@ Page Language="vb" MasterPageFile="~/PagesMaitre/VirtualiaMain.master" AutoEventWireup="false" CodeBehind="FrmConnexion.aspx.vb" 
   Inherits="Virtualia.Net.FrmConnexion" UICulture="fr" %>

<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaMain.master"  %>

<asp:Content ID="CadreConnexion" runat="server" ContentPlaceHolderID="CorpsMaster">
     <asp:Table ID="CadreSaisie" runat="server" HorizontalAlign="Center" BackColor="Transparent" CellPadding="0" CellSpacing="0"
        Height="850px" Width="1150px" BackImageUrl="~/Images/Fonds/FondConnexion.jpg"
        style="margin-top:0px; border-width: 1px; border-style: none; border-color: #00CCFF;" >
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top" HorizontalAlign="Right" Height="50px" Width="450px">
                <asp:Label ID="EtiBienvenue" runat="server" Text="Bienvenue dans Virtualia.Net" Height="50px" Width="250px"
                    BackColor="Transparent" ForeColor="#124545"
                    Font-Bold="True" Font-Names="Times New Roman" Font-Size="XX-Large"
                    style="margin-top: 50px;margin-right: 20px; font-style: oblique; text-indent: 5px; text-align: center" >
                </asp:Label>    
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Middle">
                <asp:Table ID="CadreCnx" runat="server" Height="100px" Width="470px" HorizontalAlign="Center" BackColor="#52BFB3">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" Height="25px" Width="400px">
                            <asp:Label ID="Titre" runat="server" Text="SE CONNECTER" Height="25px" Width="200px"
                                BackColor="Transparent" ForeColor="#124545"
                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Large"
                                style="margin-top: 10px; font-style: normal; text-indent: 5px; text-align: center;" >
                            </asp:Label>     
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:Table ID="CadreNom" runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Label ID="EtiNom" runat="server" Height="20px" Width="120px" Text="Utilisateur"
                                                BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                                BorderWidth="2px" ForeColor="#124545" Font-Italic="true"
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                style="margin-top: 12px; text-indent: 5px; text-align: left" >
                                        </asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="Nom" runat="server" Height="16px" Width="200px" MaxLength="35"
                                                BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset" TabIndex="1"
                                                BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                                style="margin-left: 1px; margin-top: 10px; text-indent: 1px; text-align: left" TextMode="SingleLine"> 
                                        </asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                        <asp:TableRow>
                        <asp:TableCell ID="CellPrenom" HorizontalAlign="Center" VerticalAlign="Top">
                            <asp:Table ID="CadrePrenom" runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Label ID="EtiPrenom" runat="server" Height="20px" Width="120px" Text="Prénom"
                                                BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                                BorderWidth="2px" ForeColor="#124545" Font-Italic="true"
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                style="margin-top: 5px; text-indent: 5px; text-align: left" >
                                        </asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="Prenom" runat="server" Height="16px" Width="200px" MaxLength="35"
                                                BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset" TabIndex="1"
                                                BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                                style="margin-left: 1px; margin-top: 4px; text-indent: 1px; text-align: left" TextMode="SingleLine"> 
                                        </asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                            <asp:Table ID="CadrePassword" runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Label ID="EtiPassword" runat="server" Height="20px" Width="200px" Text="Mot de passe"
                                                BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                                BorderWidth="2px" ForeColor="#124545" Font-Italic="true"
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                style="margin-top: 5px; text-indent: 5px; text-align: left" >
                                        </asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="Password" runat="server" Height="16px" Width="120px" MaxLength="16"
                                                BackColor="White" BorderColor="#B0E0D7"  BorderStyle="Inset" TabIndex="2"
                                                BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false"
                                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                                                style="margin-left: 1px; margin-top: 4px; text-indent: 1px; text-align: left" TextMode="Password"> 
                                        </asp:TextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Height="50px" Width="470px" HorizontalAlign="Center" VerticalAlign="Middle">
                            <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/General/Cmd_Std.bmp" Width="70px"
                                        BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        CellPadding="0" CellSpacing="0" Visible="true" Style="margin-top:10px" >
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                                BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" TabIndex="3" 
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                BorderStyle="None" style=" margin-left: 6px;text-align: center;">
                                            </asp:Button>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>    
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Height="30px" VerticalAlign="Top" HorizontalAlign="Center">
                <asp:Label ID="EtiMessage" runat="server" Text="" Height="25px" Width="700px"
                    BackColor="Transparent" ForeColor="#9B171A"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Large"
                    style="margin-top: 1px; margin-left: 50px; margin-bottom: 0px;
                    font-style: normal; text-indent: 5px; text-align: center;" >
                </asp:Label>     
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table ID="CadreCookie" runat="server">
                    <asp:TableRow>
                        <asp:TableCell Height="25px">
                            <asp:CheckBox ID="CocheCookieIde" runat="server" Text="Se souvenir de moi" Visible="true" AutoPostBack="true"
                                BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None" BorderWidth="2px"
                                ForeColor="#124545" Height="20px" Width="500px" Checked="false" 
                                ToolTip="Virtualia retient votre nom et votre prénom afin d'éviter une ressaisie"
                                Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 50px; font-style: oblique; text-indent: 5px; text-align: left" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow VerticalAlign="Top">
                        <asp:TableCell Height="25px">
                            <asp:CheckBox ID="CocheCookiePw" runat="server" Text="Se souvenir également de mon mot de passe" 
                                Visible="true" AutoPostBack="true"
                                BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None" BorderWidth="2px"
                                ForeColor="#124545" Height="20px" Width="500px" Checked="false"
                                ToolTip="Virtualia retient votre mot de passe afin d'éviter une ressaisie. Ceci est déconseillé si 
                                vous n'êtes pas le seul utiliser cet ordinateur."
                                Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top: 0px; margin-left: 50px; font-style: oblique; text-indent: 5px; text-align: left" />
                        </asp:TableCell>
                    </asp:TableRow>
                 </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Height="25px">
                <asp:HyperLink ID="LienModifierPw" runat="server" Text="Changer de mot de passe"
                    BackColor="Transparent" BorderStyle="None" Width="500px"
                    Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="#124545" Font-Bold="true"
                    Font-Underline="true" Font-Italic="true" Tooltip="" Visible="false"
                    style="margin-left: 50px; margin-top: 5px" /> 
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Height="25px">
                <asp:HyperLink ID="LienOubliPw" runat="server" Text="Mot de passe oublié"
                    BackColor="Transparent" BorderStyle="None" Width="500px"
                    Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="#124545" Font-Bold="true"
                    Font-Underline="true" Font-Italic="true" Tooltip="" Visible="false"
                    style="margin-left: 50px; margin-top: 5px" /> 
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>