﻿<%@ Page Language="vb" MasterPageFile="~/PagesMaitre/VirtualiaMain.master" AutoEventWireup="false" 
    CodeBehind="FrmMenuPrincipal.aspx.vb" Inherits="Virtualia.Net.FrmMenuPrincipal" MaintainScrollPositionOnPostback="True" %>

<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaMain.master"  %>
<%@ Register src="~/Controles/Standards/VMenuIcones.ascx" tagname="VMenuI" tagprefix="Virtualia" %>

<asp:Content ID="CadreEntete" runat="server" ContentPlaceHolderID="InfoGenMaster" Visible="false">
</asp:Content>
<asp:Content ID="CadreGeneral" runat="server" ContentPlaceHolderID="CorpsMaster">
    <asp:Table ID="CadreVues" runat="server" Width="1150px" HorizontalAlign="Center"
                     BackColor="#216B68" CellSpacing="2" style="margin-top: 2px ">
        <asp:TableRow>
            <asp:TableCell ID="ConteneurVues" Width="1150px" Height="800px" VerticalAlign="Top" HorizontalAlign="Center">
                <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                    <asp:View ID="VueMenuIcone" runat="server">  
                        <Virtualia:VMenuI ID="MenuTactile" runat="server" />
                    </asp:View> 
                </asp:MultiView>
            </asp:TableCell> 
        </asp:TableRow>
    </asp:Table>
</asp:Content>