﻿Imports Microsoft.VisualBasic
Public Class VirWebResponse
    Public Shared Sub Redirect(ByVal Url As String, ByVal Target As String, ByVal WindowFeatures As String)
        Dim Contexte As HttpContext = HttpContext.Current

        If (String.IsNullOrEmpty(Target) OrElse Target.Equals("_self", StringComparison.OrdinalIgnoreCase)) AndAlso String.IsNullOrEmpty(WindowFeatures) Then
            Contexte.Response.Redirect(Url)
        Else
            Dim Frm As Page = CType(Contexte.Handler, Page)
            Dim ScriptOuverture As String

            If Frm Is Nothing Then
                Throw New InvalidOperationException("Impossible d'ouvrir une nouvelle page.")
            End If
            Url = Frm.ResolveClientUrl(Url)

            If (Not String.IsNullOrEmpty(WindowFeatures)) Then
                ScriptOuverture = "window.open(""{0}"", ""{1}"", ""{2}"");"
            Else
                ScriptOuverture = "window.open(""{0}"", ""{1}"");"
            End If
            ScriptOuverture = String.Format(ScriptOuverture, Url, Target, WindowFeatures)

            ScriptManager.RegisterStartupScript(Frm, GetType(Page), "Redirect", ScriptOuverture, True)

        End If

    End Sub
    '*** Aide Les options de la fenêtre windowFeatures sont les suivantes: 
    'directories = yes/no - Affiche ou non les boutons de navigation
    'location = yes/no    - Affiche ou non la barre d'adresse
    'menubar = yes/no     - Affiche ou non la barre de menu (fichier, edition, ...)
    'resizable = yes/no   - Définit si la taille de la fenêtre est modifiable ou non
    'scrollbars = yes/no  - Affiche ou non les ascenceurs (barres de défilement)
    'status = yes/no      - Affiche ou non la barre d'état
    'toolbar = yes/no     - Affiche ou non la barre d'outils
    'width = largeur (en pixels) - Définit la largeur
    'height = hauteur (en pixels) - Définit la hauteur
    'Ainsi, il est possible d'utiliser cette méthode avec n'importe quel gestionnaire d'événement, directement dans le code à exécuter ou bien dans une fonction.
    '•les options doivent être saisies les unes après les autres, séparées par des virgules, sans espace
    '•l'ensemble des options doit être encadré par les guillemets
    'Chacune des fenêtres doit cependant être fermée, il faut donc se servir de la méthode close() qui permet de fermer une fenêtre.


End Class
