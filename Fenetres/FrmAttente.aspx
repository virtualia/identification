﻿<%@ Page Language="vb" MasterPageFile="~/PagesMaitre/VirtualiaMain.master" AutoEventWireup="false" CodeBehind="FrmAttente.aspx.vb" 
    Inherits="Virtualia.Net.FrmAttente" %>

<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaMain.master"  %>

<asp:Content ID="CadreEntete" runat="server" ContentPlaceHolderID="InfoGenMaster" Visible="false">
</asp:Content>
<asp:Content ID="CadreGeneral" runat="server" ContentPlaceHolderID="CorpsMaster">
    <asp:UpdatePanel ID="UpdatePanelAttente" runat="server">
        <ContentTemplate>
            <asp:Timer ID="TimerAttente" runat="server" Interval="1000">
            </asp:Timer>
            <asp:Table ID="CadreAttente" runat="server" BackColor="Transparent" HorizontalAlign="Center" Width="1150px" Height="850px"
                       Style="margin-top: 1px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Middle" ForeColor="White" Font-Bold="false" Height="60px">
                        <asp:UpdateProgress ID="UpdateAttente" runat="server">
                            <ProgressTemplate>
                                    <img alt="" src="../Images/General/Loading.gif" 
                                        style="vertical-align:top; text-align:center; font-family: 'Trebuchet MS'; height: 30px" />
                                        Chargement de l'application - veuillez patienter ... 
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </asp:TableCell>
                 </asp:TableRow>
            </asp:Table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
