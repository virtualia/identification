﻿Imports System.Web.SessionState

Public Class Global_asax
    Inherits System.Web.HttpApplication
    Private VirObjetGlobal As Virtualia.Net.Session.ObjetGlobal

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lorsque l'application est démarrée
        Dim RhOrdi As Virtualia.Systeme.Outils.Ordinateur
        Dim WcfServeur As Virtualia.Net.ServiceServeur.VirtualiaServeurClient
        Dim Uti As Virtualia.Net.ServiceServeur.UtilisateurType
        Dim TableauData(0) As String

        Uti = New Virtualia.Net.ServiceServeur.UtilisateurType
        RhOrdi = New Virtualia.Systeme.Outils.Ordinateur
        Uti.Nom = System.Configuration.ConfigurationManager.AppSettings("NomUtilisateur")
        Uti.Machine = RhOrdi.NomdelOrdinateur
        Uti.AdresseIP = RhOrdi.AdresseIPOrdinateur
        Uti.MotPasse = System.Configuration.ConfigurationManager.AppSettings("Password")
        Uti.InstanceBd = CInt(System.Configuration.ConfigurationManager.AppSettings("NumeroDatabase"))

        WcfServeur = New Virtualia.Net.ServiceServeur.VirtualiaServeurClient
        Try
            Select Case WcfServeur.OuvertureSession(Uti)
                Case Virtualia.Systeme.Constantes.CnxUtilisateur.CnxOK, Virtualia.Systeme.Constantes.CnxUtilisateur.CnxDejaConnecte, Virtualia.Systeme.Constantes.CnxUtilisateur.CnxReprise, Virtualia.Systeme.Constantes.CnxUtilisateur.CnxVirtualia
                    Application.Add("Database", Uti.InstanceBd)
                Case Else
                    Call EcrireLog("---- Erreur à la connexion au ServiceServeur ---" & System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaServeur"))
                    Me.Dispose()
                    Exit Sub
            End Select
        Catch ex As Exception
            Call EcrireLog("---- Erreur ServiceServeur ne répond pas ---" & System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaServeur") & Strings.Space(1) & ex.Message)
            Me.Dispose()
            Exit Sub
        End Try

        Try
            VirObjetGlobal = New Virtualia.Net.Session.ObjetGlobal(Uti.Nom, Uti.InstanceBd)
            Application.Add("VirGlobales", VirObjetGlobal)
        Catch ex As Exception
            Call EcrireLog("---- Erreur au Chargement de l'application ---" & Strings.Space(1) & ex.Message)
            Me.Dispose()
            Exit Sub
        End Try

    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lorsque la session est démarrée
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche au début de chaque demande
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lors d'une tentative d'authentification de l'utilisation
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lorsqu'une erreur se produit
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lorsque la session se termine
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lorsque l'application se termine
        Dim ChemindeBase As String
        Dim NomRepertoire As String
        Dim SousRepertoire As System.Collections.ObjectModel.ReadOnlyCollection(Of String)
        Dim IndiceI As Integer

        Try
            ChemindeBase = Request.PhysicalApplicationPath
            If Strings.Right(ChemindeBase, 1) <> "\" Then
                ChemindeBase &= "\"
            End If
            NomRepertoire = ChemindeBase & "Telechargement"
            If My.Computer.FileSystem.DirectoryExists(NomRepertoire) = True Then
                SousRepertoire = My.Computer.FileSystem.GetDirectories(NomRepertoire)
                If SousRepertoire Is Nothing Then
                    Exit Sub
                End If
                For IndiceI = 0 To SousRepertoire.Count - 1
                    Try
                        My.Computer.FileSystem.DeleteDirectory(SousRepertoire(IndiceI), FileIO.DeleteDirectoryOption.DeleteAllContents)
                    Catch ex As Exception
                        Exit Try
                    End Try
                Next IndiceI
            End If
        Catch ex As Exception
            Exit Try
        End Try
    End Sub

    Private Sub EcrireLog(ByVal Msg As String)
        Dim FicStream As System.IO.FileStream
        Dim FicWriter As System.IO.StreamWriter
        Dim NomLog As String
        Dim SysFicLog As String = "WcfServiceSessions.log"
        Dim SysCodeIso As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)

        NomLog = Virtualia.Systeme.Constantes.DossierVirtualiaService("Logs") & SysFicLog
        FicStream = New System.IO.FileStream(NomLog, IO.FileMode.Append, IO.FileAccess.Write)
        FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)
        FicWriter.WriteLine(Format(System.DateTime.Now, "g") & Space(1) & Msg)
        FicWriter.Flush()
        FicWriter.Close()
        FicStream.Close()
    End Sub
End Class