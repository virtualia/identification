﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class FrmMenuPrincipal
    Inherits System.Web.UI.Page

    Private Sub FrmMenuPrincipal_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Dim NumSession As String = Me.Master.V_NoSession
        If NumSession = "" Then
            NumSession = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
            Me.Master.V_NoSession = NumSession
        End If
        If NumSession = "" Then
            Dim Msg As String = "Erreur lors de l'identification."
            Response.Redirect("~/Fenetres/Connexion/ErreurApplication.aspx?Msg=" & Msg)
        End If
    End Sub

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Dim ChaineVisible As String = System.Configuration.ConfigurationManager.AppSettings("IconesVisibles")
        Dim TabVisible As List(Of String)
        Dim IndiceK As Integer
        If ChaineVisible = "" Then
            Exit Sub
        End If
        For IndiceK = 1 To 13
            MenuTactile.SiIconeEnabled(IndiceK) = False
        Next IndiceK
        TabVisible = Strings.Split(ChaineVisible, VI.PointVirgule, -1).ToList
        For Each Indice In TabVisible
            MenuTactile.SiIconeEnabled(CInt(Indice)) = True
        Next
    End Sub

    Private Sub MenuTactile_CmdIcone_Click(sender As Object, e As Systeme.Evenements.AppelChoixEventArgs) Handles MenuTactile.CmdIcone_Click
        If e.Valeurs Is Nothing Then
            Exit Sub
        End If
        Dim TabValeurs As ArrayList = e.Valeurs
        If IsNumeric(TabValeurs(0)) Then
            Me.Master.V_Logo = CInt(TabValeurs(0))
        End If
    End Sub

End Class