﻿Option Strict On
Option Explicit On
Option Compare Text
Public Class FrmAttente
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsNomState As String = "VAttente"

    Public Property V_Index As Integer
        Get
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
                If IsNumeric(VCache(0)) Then
                    Return CInt(VCache(0))
                End If
            End If
            Return 0
        End Get
        Set(ByVal value As Integer)
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) Is Nothing Then
                VCache = New ArrayList
                VCache.Add(value)
                VCache.Add("")
                VCache.Add("")
            Else
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
                If IsNumeric(VCache(0)) Then
                    If CInt(VCache(0)) = value Then
                        Exit Property
                    End If
                End If
                VCache(0) = value
                Me.ViewState.Remove(WsNomState)
            End If
            Me.ViewState.Add(WsNomState, VCache)
        End Set
    End Property

    Public Property V_UrlCible As String
        Get
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
                Return VCache(1).ToString
            End If
            Return ""
        End Get
        Set(ByVal value As String)
            If value = "" Then
                Exit Property
            End If
            Dim VCache As ArrayList
            VCache = CType(Me.ViewState(WsNomState), ArrayList)
            If VCache Is Nothing Then
                Exit Property
            End If
            Me.ViewState.Remove(WsNomState)
            VCache(1) = value
            Me.ViewState.Add(WsNomState, VCache)
        End Set
    End Property

    Public Property V_Parametre As String
        Get
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
                Return VCache(2).ToString
            End If
            Return ""
        End Get
        Set(ByVal value As String)
            If value = "" Then
                Exit Property
            End If
            Dim VCache As ArrayList
            VCache = CType(Me.ViewState(WsNomState), ArrayList)
            If VCache Is Nothing Then
                Exit Property
            End If
            Me.ViewState.Remove(WsNomState)
            VCache(2) = value
            Me.ViewState.Add(WsNomState, VCache)
        End Set
    End Property

    Private Sub FrmAttente_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim NumSession As String = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
        Dim NumLogo As String = Server.HtmlDecode(Request.QueryString("Index"))
        Dim LienCible As String = Server.HtmlDecode(Request.QueryString("Lien"))
        Dim Param As String = Server.HtmlDecode(Request.QueryString("Param"))
        If Param Is Nothing Then
            Param = ""
        End If

        If NumSession = "" Then
            NumSession = Me.Master.V_NoSession
        End If
        WebFct = New Virtualia.Net.WebFonctions(Me, 1)
        If NumSession = "" Or LienCible = "" Then
            Response.Redirect("~/Fenetres/Connexion/FrmDeconnexion.aspx")
        End If
        If IsNumeric(NumLogo) Then
            V_Index = CInt(NumLogo)
        Else
            NumLogo = "0"
        End If
        Select Case Strings.Left(LienCible, 4)
            Case "http"
                V_UrlCible = LienCible
            Case Else
                V_UrlCible = "~/Fenetres/" & LienCible & "?IDVirtualia=" & NumSession
        End Select
        If Param <> "" Then
            V_UrlCible &= "&Param=" & Param
        End If

        Dim Charte As New Virtualia.Systeme.Fonctions.CharteGraphique
        CadreAttente.BackColor = Charte.CouleurTuile(CInt(NumLogo))
    End Sub

    Private Sub TimerAttente_Tick(sender As Object, e As EventArgs) Handles TimerAttente.Tick
        Dim ChaineHttp As String = V_UrlCible
        If ChaineHttp = "" Then
            Exit Sub
        End If
        V_UrlCible = ""
        Response.Redirect(ChaineHttp)
    End Sub
End Class