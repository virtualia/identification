﻿Public Class FrmDeconnexion
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ChaineSession As String = Session.Item("IDVirtualia").ToString
        If ChaineSession <> "" Then
            Dim WebFct As New Virtualia.Net.WebFonctions(Me, 1)
            Call WebFct.PointeurGlobal.Deconnexion(ChaineSession)
            Response.Redirect("~/Fenetres/Connexion/FrmConnexion.aspx")
        End If
    End Sub

End Class