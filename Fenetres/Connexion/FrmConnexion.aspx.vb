﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class FrmConnexion
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsCtl_Cache As Virtualia.Net.VCaches.CacheIdentification
    Private WsNomStateCache As String = "CnxUtilisateur"

    Private ReadOnly Property V_WebFonction As Virtualia.Net.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.WebFonctions(Me, 1)
            End If
            Return WebFct
        End Get
    End Property

    Private Property CacheVirControle As Virtualia.Net.VCaches.CacheIdentification
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.VCaches.CacheIdentification)
            End If
            Dim NewCache As Virtualia.Net.VCaches.CacheIdentification
            NewCache = New Virtualia.Net.VCaches.CacheIdentification
            NewCache.ModedeConnexion = ""
            Return NewCache
        End Get
        Set(value As Virtualia.Net.VCaches.CacheIdentification)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Private Sub EcrireCookie()
        Dim Vcookie As System.Web.HttpCookie

        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.ModedeConnexion = V_WebFonction.PointeurGlobal.VirModeConnexion
        If WsCtl_Cache.Nom_Identification.ToUpper.Contains("VIRTUALIA") = True Then
            WsCtl_Cache.ModedeConnexion = "V4"
        ElseIf My.Computer.FileSystem.FileExists(System.Configuration.ConfigurationManager.AppSettings("RepertoireUtilisateurV3") & "\" & WsCtl_Cache.Nom_Identification & ".Xml") Then
            WsCtl_Cache.ModedeConnexion = "V3"
        End If
        WsCtl_Cache.NumeroSession = Session.SessionID
        CacheVirControle = WsCtl_Cache

        Vcookie = Request.Cookies.Get("MyVirtualia")
        If Vcookie Is Nothing Then
            Vcookie = New System.Web.HttpCookie("MyVirtualia")
        End If
        Vcookie.Value = V_WebFonction.EncodageCookie(WsCtl_Cache.Chaine_Cookie)
        Vcookie.Expires = DateTime.Now.AddMonths(3)
        Response.Cookies.Add(Vcookie)
    End Sub

    Private Sub LireCookie()
        Dim Vcookie As System.Web.HttpCookie = Nothing

        Vcookie = Request.Cookies.Get("MyVirtualia")
        If Vcookie Is Nothing OrElse Vcookie.Value Is Nothing Then
            Exit Sub
        End If
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Chaine_Cookie = V_WebFonction.DecodageCookie(Vcookie.Value)
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub FrmConnexion_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.Master.SiFRmCnx = True
        LienModifierPw.Visible = False
        LienOubliPw.Visible = False
    End Sub

    Private Sub FrmConnexion_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Session.Count = 0 Then
            Response.Redirect("~/Default.aspx")
        End If
        Dim SiPossible As Boolean = False

        WsCtl_Cache = CacheVirControle
        If WsCtl_Cache.ModedeConnexion = "" Then
            Call LireCookie()
        End If
        If WsCtl_Cache.ModedeConnexion = "" Then
            WsCtl_Cache.ModedeConnexion = V_WebFonction.PointeurGlobal.VirModeConnexion
        End If
        CacheVirControle = WsCtl_Cache
        Nom.Text = WsCtl_Cache.Nom_Identification
        Select Case V_WebFonction.PointeurGlobal.VirModeConnexion
            Case "VI" 'Intranet Cnx Standard
                Prenom.Text = WsCtl_Cache.Prenom
                CellPrenom.Visible = True
            Case Else
                EtiNom.Text = "Nom usuel"
                Prenom.Text = ""
                CellPrenom.Visible = False
        End Select
        If V_WebFonction.PointeurGlobal.VirModeConnexion = "AD" Then
            LienModifierPw.Visible = False
            LienOubliPw.Visible = False
        End If
        CocheCookieIde.Checked = WsCtl_Cache.SiSeSouvenir_Identification
        CocheCookiePw.Checked = WsCtl_Cache.SiSeSouvenir_Password

        Select Case WsCtl_Cache.ModedeConnexion
            Case "VI"
                If WsCtl_Cache.Nom_Identification <> "" And WsCtl_Cache.Prenom <> "" Then
                    SiPossible = True
                End If
            Case Else
                If WsCtl_Cache.Nom_Identification <> "" Then
                    SiPossible = True
                End If
        End Select
        If SiPossible = True Then
            LienModifierPw.NavigateUrl = "~/Fenetres/Connexion/FrmMotdePasse.aspx?IDVirtualia=" & Server.HtmlEncode(Session.SessionID) & "&Mode=1"
            LienModifierPw.NavigateUrl &= "&Nom=" & Server.HtmlEncode(WsCtl_Cache.Nom_Identification) & "&Prenom=" & Server.HtmlEncode(WsCtl_Cache.Prenom)
            LienOubliPw.NavigateUrl = "~/Fenetres/Connexion/FrmMotdePasse.aspx?IDVirtualia=" & Server.HtmlEncode(Session.SessionID) & "&Mode=2"
            LienOubliPw.NavigateUrl &= "&Nom=" & Server.HtmlEncode(WsCtl_Cache.Nom_Identification) & "&Prenom=" & Server.HtmlEncode(WsCtl_Cache.Prenom)
        End If
    End Sub

    Protected Sub Nom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Nom.TextChanged
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Nom_Identification = V_WebFonction.ViRhFonction.Lettre1Capi(Nom.Text, 2)
        CacheVirControle = WsCtl_Cache
        EtiMessage.Text = ""
        Prenom.Focus()
    End Sub

    Private Sub Prenom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Prenom.TextChanged
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Prenom = V_WebFonction.ViRhFonction.Lettre1Capi(Prenom.Text, 1)
        CacheVirControle = WsCtl_Cache
        EtiMessage.Text = ""
        Password.Focus()
    End Sub

    Protected Sub Password_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Password.TextChanged
        If Password.Text.Trim = "" Then
            Exit Sub
        End If
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.MotdePasseCrypte = V_WebFonction.ViRhFonction.HashSHA(Password.Text.Trim)
        If WsCtl_Cache.ModedeConnexion = "V3" Then
            WsCtl_Cache.MotdePasseCrypte = V_WebFonction.ViRhFonction.HashSHA(Password.Text.ToUpper.Trim)
        End If
        CacheVirControle = WsCtl_Cache
        EtiMessage.Text = ""
        CommandeOK.Focus()
    End Sub

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Dim Msg As String = ""
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.ModedeConnexion = V_WebFonction.PointeurGlobal.VirModeConnexion
        If WsCtl_Cache.Nom_Identification.ToUpper.Contains("VIRTUALIA") Then
            WsCtl_Cache.ModedeConnexion = "V4"
        ElseIf My.Computer.FileSystem.FileExists(System.Configuration.ConfigurationManager.AppSettings("RepertoireUtilisateurV3") & "\" & WsCtl_Cache.Nom_Identification & ".Xml") Then
            WsCtl_Cache.ModedeConnexion = "V3"
        End If
        Select Case WsCtl_Cache.ModedeConnexion
            Case "VI"
                If WsCtl_Cache.Nom_Identification = "" Or WsCtl_Cache.Prenom = "" Or WsCtl_Cache.MotdePasseCrypte = "" Then
                    Msg = "Le nom, le prénom et le mot de passe sont obligatoires"
                    EtiMessage.Text = Msg
                    Exit Sub
                End If
            Case Else
                If WsCtl_Cache.Nom_Identification = "" Or WsCtl_Cache.MotdePasseCrypte = "" Then
                    Msg = "L'identification et le mot de passe sont obligatoires"
                    EtiMessage.Text = Msg
                    Exit Sub
                End If
        End Select
        Dim SiOK As Boolean = False
        Dim ChaineCnx As String = Session.SessionID & VI.PointVirgule
        ChaineCnx &= Session.Item("Machine").ToString & VI.PointVirgule
        ChaineCnx &= Session.Item("IP").ToString & VI.PointVirgule
        ChaineCnx &= Session.Item("Identite").ToString
        Dim ChaineIdent As String = WsCtl_Cache.Nom_Identification
        Select Case WsCtl_Cache.ModedeConnexion
            Case "VI"
                ChaineIdent &= VI.PointVirgule & WsCtl_Cache.Prenom
        End Select
        Try
            SiOK = V_WebFonction.PointeurGlobal.SiConnexionAutorisee(ChaineCnx, ChaineIdent, WsCtl_Cache.MotdePasseCrypte, WsCtl_Cache.ModedeConnexion)
            ''*** AKR 
            'V_WebFonction.PointeurGlobal.VirMdp = Session.SessionID & VI.PointVirgule & ChaineIdent & VI.PointVirgule & Password.Text.Trim
            ''*** fin AKR
        Catch ex As Exception
            SiOK = False
        End Try
        If SiOK = False Then
            EtiMessage.Text = "Connexion refusée - Identification invalide"
            Exit Sub
        End If
        CacheVirControle = WsCtl_Cache
        Call EcrireCookie()

        Response.Redirect("~/Fenetres/Connexion/FrmLoginValide.aspx?IDVirtualia=" & Server.HtmlEncode(Session.SessionID))
    End Sub

    Private Sub CocheCookieIde_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CocheCookieIde.CheckedChanged
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.SiSeSouvenir_Identification = CType(sender, System.Web.UI.WebControls.CheckBox).Checked
        CacheVirControle = WsCtl_Cache
        Call EcrireCookie()
    End Sub

    Private Sub CocheCookiePw_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CocheCookiePw.CheckedChanged
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.SiSeSouvenir_Password = CType(sender, System.Web.UI.WebControls.CheckBox).Checked
        CacheVirControle = WsCtl_Cache
        Call EcrireCookie()
    End Sub

    Private Sub PurgerTelechargement(ByVal Ide As Integer)
        Dim ChemindeBase As String
        Dim NomRepertoire As String

        ChemindeBase = Request.PhysicalApplicationPath
        If Strings.Right(ChemindeBase, 1) <> "\" Then
            ChemindeBase &= "\"
        End If
        NomRepertoire = ChemindeBase & "Telechargement\" & Ide.ToString
        If My.Computer.FileSystem.DirectoryExists(NomRepertoire) = True Then
            Try
                My.Computer.FileSystem.DeleteDirectory(NomRepertoire, FileIO.DeleteDirectoryOption.DeleteAllContents)
            Catch ex As Exception
                Exit Try
            End Try
        End If
    End Sub
End Class
