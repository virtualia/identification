﻿<%@ Page Language="vb" MasterPageFile="~/PagesMaitre/VirtualiaMain.Master" AutoEventWireup="false" CodeBehind="FrmLoginValide.aspx.vb" 
     Inherits="Virtualia.Net.FrmLoginValide" UICulture="fr" %>

<%@ MasterType VirtualPath="~/PagesMaitre/VirtualiaMain.Master" %>

<asp:Content ID="CadreValidation" runat="server" ContentPlaceHolderID="CorpsMaster">
    <asp:UpdatePanel ID="UpdatePanelValide" runat="server">
        <ContentTemplate>
            <asp:Timer ID="HorlogeLogin" runat="server" Interval="1000">
            </asp:Timer>
            <asp:Table ID="CadreAttente" runat="server" Height="850px" Width="1150px" HorizontalAlign="Center"
                                BackColor="#124545" style="margin-top: 0px; margin-bottom: 0px; border-width: 4px; border-style: ridge;
                                border-color: #B0E0D7" >
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ForeColor="White" Height="60px">
                        <asp:UpdateProgress ID="UpdateAttente" runat="server">
                                <ProgressTemplate>
                                    <img alt="" src="../../Images/General/Loading.gif" 
                                    style="vertical-align:top; text-align:center; font-family: 'Trebuchet MS';" 
                                    height="30px" /> 
                                    Chargement de l'application - Traitement en cours ... 
                                </ProgressTemplate>
                        </asp:UpdateProgress>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="40px" HorizontalAlign="Center" VerticalAlign="Top">
                        <asp:Label ID="EtiCnx1" runat="server" Text="Connexion autorisée."
                                        Height="30px" Width="1000px" BackColor="Transparent" ForeColor="#D7FAF3"
                                        BorderColor="#B6C7E2"  BorderStyle="None" BorderWidth="1px"
                                        Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Large" Font-Italic="true">
                         </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Height="100px" HorizontalAlign="Center" VerticalAlign="Top">
                        <asp:Label ID="EtiCnx2" runat="server" Text="Veuillez patienter..."
                                        Height="30px" Width="1000px" BackColor="Transparent" ForeColor="#D7FAF3"
                                        BorderColor="#B6C7E2"  BorderStyle="None" BorderWidth="1px"
                                        Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Large" Font-Italic="true">
                         </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                        <asp:Label ID="EtiTraitement" runat="server" Text="Traitement en cours..."
                                        Height="30px" Width="1000px" BackColor="Transparent" ForeColor="#D7FAF3"
                                        BorderColor="#B6C7E2"  BorderStyle="None" BorderWidth="1px"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Large" Font-Italic="true">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:HiddenField ID="HNumPage" runat="server" Value="0" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </ContentTemplate>
    </asp:UpdatePanel>     
</asp:Content>  