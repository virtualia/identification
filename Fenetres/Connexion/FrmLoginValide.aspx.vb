﻿Option Compare Text
Public Class FrmLoginValide
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.WebFonctions

    Private Sub FrmLoginValide_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        WebFct = New Virtualia.Net.WebFonctions(Me, 1)
        Dim AppObjetGlobal As Virtualia.Net.Session.ObjetGlobal = WebFct.PointeurGlobal
        Dim IdSession As String = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
        Dim NomUti As String = AppObjetGlobal.IDUtilisateur(IdSession)

        If NomUti = "" Then
            Dim Msg As String = "Erreur lors de l'authentification du dossier accédé."
            Response.Redirect("~/Fenetres/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        Call AppObjetGlobal.EcrireLogTraitement("WcfConnexions", True, "Identification - " & NomUti & " - " & IdSession)
        Response.Redirect("~/Fenetres/Menus/FrmMenuPrincipal.aspx?IDVirtualia=" & Server.HtmlEncode(IdSession))
    End Sub
End Class