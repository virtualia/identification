﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class FrmMotdePasse
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.WebFonctions
    Private WsRhFonction As Virtualia.Systeme.Fonctions.Generales
    Private WsNomStateUti As String = "Utilisateur"
    Private WsCacheUti As List(Of String)
    Private WsNomStateValid As String = "Validation"
    Private WsCacheValid As List(Of String)

    Private ReadOnly Property V_WebFonction As Virtualia.Net.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.WebFonctions(Me, 1)
            End If
            Return WebFct
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.ViewState(WsNomStateUti) Is Nothing Then
            Dim ChaineSession As String = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
            Dim Nom As String = Server.HtmlDecode(Request.QueryString("Nom"))
            Dim Prenom As String = Server.HtmlDecode(Request.QueryString("Prenom"))
            If ChaineSession = "" Or Nom = "" Then
                Dim Msg As String = "Erreur lors de l'identification du dossier accédé."
                Response.Redirect("~/Fenetres/ErreurApplication.aspx?Msg=" & Msg)
                Exit Sub
            End If
            WsCacheUti = New List(Of String)
            WsCacheUti.Add(Nom)
            WsCacheUti.Add(Prenom)
            WsCacheUti.Add(Request.QueryString("Mode"))
        Else
            WsCacheUti = CType(Me.ViewState(WsNomStateUti), List(Of String))
        End If
        Me.ViewState.Add(WsNomStateUti, WsCacheUti)
        Titre.Text = WsCacheUti.Item(0) & Strings.Space(1) & WsCacheUti.Item(1)
        If WsCacheUti.Item(2) = "1" Then
            VPassword.Visible = True
            VEMail.Visible = False
        Else
            VPassword.Visible = False
            VEMail.Visible = True
        End If
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Me.ViewState(WsNomStateValid) IsNot Nothing Then
            WsCacheValid = CType(Me.ViewState(WsNomStateValid), List(Of String))
        End If
        If WsCacheValid Is Nothing Then
            Exit Sub
        End If
        If WsCacheUti.Item(2) = "1" Then
            If WsCacheValid.Item(0) <> "" Then
                Try
                    OldPassword.Text = WsCacheValid.Item(0)
                Catch ex As Exception
                    OldPassword.Text = ""
                End Try
            End If
            If WsCacheValid(1) <> "" Then
                Try
                    NewPassword.Text = WsCacheValid.Item(1)
                Catch ex As Exception
                    NewPassword.Text = ""
                End Try
            End If
            If WsCacheValid(2) <> "" Then
                Try
                    NewConfirmation.Text = WsCacheValid.Item(2)
                Catch ex As Exception
                    NewConfirmation.Text = ""
                End Try
            End If
        Else
            If WsCacheValid.Item(0) <> "" Then
                Try
                    EMailPro.Text = WsCacheValid.Item(0)
                Catch ex As Exception
                    EMailPro.Text = ""
                End Try
            End If
            If WsCacheValid(1).ToString <> "" Then
                Try
                    EMailConfirme.Text = WsCacheValid.Item(1)
                Catch ex As Exception
                    EMailConfirme.Text = ""
                End Try
            End If
        End If
    End Sub

    Private Sub EMailPro_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles EMailPro.TextChanged
        If Me.ViewState(WsNomStateValid) IsNot Nothing Then
            WsCacheValid = CType(Me.ViewState(WsNomStateValid), List(Of String))
            WsCacheValid.Item(0) = EMailPro.Text
            Me.ViewState.Remove(WsNomStateValid)
        Else
            WsCacheValid = New List(Of String)
            WsCacheValid.Add(EMailPro.Text)
            WsCacheValid.Add("")
        End If
        Me.ViewState.Add(WsNomStateValid, WsCacheValid)
        EtiMessage.Text = ""
        EMailConfirme.Focus()
    End Sub

    Private Sub EMailConfirme_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles EMailConfirme.TextChanged
        If Me.ViewState(WsNomStateValid) IsNot Nothing Then
            WsCacheValid = CType(Me.ViewState(WsNomStateValid), List(Of String))
            WsCacheValid.Item(1) = EMailConfirme.Text
            Me.ViewState.Remove(WsNomStateValid)
        Else
            WsCacheValid = New List(Of String)
            WsCacheValid.Add("")
            WsCacheValid.Add(EMailConfirme.Text)
        End If
        Me.ViewState.Add(WsNomStateValid, WsCacheValid)
        EtiMessage.Text = ""
        CommandeOK.Focus()
    End Sub

    Private Sub OldPassword_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles OldPassword.TextChanged
        If WsRhFonction Is Nothing Then
            WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
        End If
        If Me.ViewState(WsNomStateValid) IsNot Nothing Then
            WsCacheValid = CType(Me.ViewState(WsNomStateValid), List(Of String))
            WsCacheValid.Item(0) = OldPassword.Text
            Me.ViewState.Remove(WsNomStateValid)
        Else
            WsCacheValid = New List(Of String)
            WsCacheValid.Add(OldPassword.Text)
            WsCacheValid.Add("")
            WsCacheValid.Add("")
        End If
        Me.ViewState.Add(WsNomStateValid, WsCacheValid)
        EtiMessage.Text = ""
        NewPassword.Focus()
    End Sub

    Private Sub NewPassword_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles NewPassword.TextChanged
        If WsRhFonction Is Nothing Then
            WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
        End If
        If Me.ViewState(WsNomStateValid) IsNot Nothing Then
            WsCacheValid = CType(Me.ViewState(WsNomStateValid), List(Of String))
            WsCacheValid.Item(1) = NewPassword.Text
            Me.ViewState.Remove(WsNomStateValid)
        Else
            WsCacheValid = New List(Of String)
            WsCacheValid.Add("")
            WsCacheValid.Add(NewPassword.Text)
            WsCacheValid.Add("")
        End If
        Me.ViewState.Add(WsNomStateValid, WsCacheValid)
        EtiMessage.Text = ""
        NewConfirmation.Focus()
    End Sub

    Private Sub NewConfirmation_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles NewConfirmation.TextChanged
        If WsRhFonction Is Nothing Then
            WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
        End If
        If Me.ViewState(WsNomStateValid) IsNot Nothing Then
            WsCacheValid = CType(Me.ViewState(WsNomStateValid), List(Of String))
            WsCacheValid.Item(2) = NewConfirmation.Text
            Me.ViewState.Remove(WsNomStateValid)
        Else
            WsCacheValid = New List(Of String)
            WsCacheValid.Add("")
            WsCacheValid.Add("")
            WsCacheValid.Add(NewConfirmation.Text)
        End If
        Me.ViewState.Add(WsNomStateValid, WsCacheValid)
        EtiMessage.Text = ""
        CommandeOK.Focus()
    End Sub

    Private Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        If Me.ViewState(WsNomStateUti) Is Nothing Then
            Exit Sub
        End If
        If Me.ViewState(WsNomStateValid) Is Nothing Then
            Exit Sub
        End If
        If WsRhFonction Is Nothing Then
            WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
        End If

        Dim Msg As String = ""
        WsCacheUti = CType(Me.ViewState(WsNomStateUti), List(Of String))
        WsCacheValid = CType(Me.ViewState(WsNomStateValid), List(Of String))
        If WsCacheUti.Item(2) = "1" Then
            If WsCacheValid.Item(0) = "" Or WsCacheValid.Item(1) = "" Or WsCacheValid.Item(2) = "" Then
                Msg = "La saisie des trois champs est obligatoire"
                EtiMessage.Text = Msg
                Exit Sub
            End If
            If WsCacheValid.Item(1) <> WsCacheValid.Item(2) Then
                Msg = "La confirmation du nouveau mot de passe n'est pas correcte"
                EtiMessage.Text = Msg
                Exit Sub
            End If
        Else
            If WsCacheValid.Item(0) <> WsCacheValid.Item(1) Then
                Msg = "La confirmation de l'adresse Email n'est pas correcte"
                EtiMessage.Text = Msg
                Exit Sub
            End If
        End If

        Dim AppObjetGlobal As Virtualia.Net.Session.ObjetGlobal = V_WebFonction.PointeurGlobal
        Dim SiOK As Boolean = False

        Dim ChaineCnx As String = Session.SessionID & VI.PointVirgule
        ChaineCnx &= Request.UserHostName & VI.PointVirgule
        ChaineCnx &= Request.UserHostAddress & VI.PointVirgule
        ChaineCnx &= Session.Item("Identite").ToString

        Dim ChaineIdent As String = WsRhFonction.HashSHA(WsCacheUti.Item(0))
        Select Case AppObjetGlobal.VirModeConnexion
            Case Is = "VI"
                ChaineIdent &= VI.PointVirgule & WsRhFonction.HashSHA(WsCacheUti.Item(1))
        End Select
        If WsCacheUti.Item(2) = "1" Then
            SiOK = AppObjetGlobal.SiConnexionAutorisee(ChaineCnx, ChaineIdent, WsRhFonction.HashSHA(WsCacheValid.Item(0)))
            If SiOK = False Then
                Msg = "Modification refusée - Ancien mot de passe invalide"
                EtiMessage.Text = Msg
                Exit Sub
            End If
            SiOK = AppObjetGlobal.SiModificationPWOK(ChaineIdent, WsRhFonction.HashSHA(WsCacheValid.Item(0)))
            If SiOK = True Then
                Msg = "Modification effectuée."
            Else
                Msg = "La modification n'a pas pu être effectuée."
            End If
            EtiMessage.Text = Msg
            Call VerifierCookie()
            Exit Sub
        Else
            Dim NewPasse As String = AppObjetGlobal.NouveauMotdePasse(ChaineIdent, WsCacheValid(0).ToString)
            If NewPasse <> "" Then
                Dim WebOutil As Virtualia.Net.ServiceOutils.VirtualiaOutilsSoapClient
                Dim ContenuMsg As String = "Vous avez demandé un nouveau mot de passe pour vous connecter à l'application Virtualia.Net." & vbCrLf
                ContenuMsg &= "Le mot de passe généré automatiquement est le suivant :   " & NewPasse & Strings.Space(3)

                WebOutil = New Virtualia.Net.ServiceOutils.VirtualiaOutilsSoapClient
                SiOK = WebOutil.EnvoyerEmail("", WsCacheValid(0).ToString, ContenuMsg, False)
                WebOutil.Close()

                If SiOK = True Then
                    Msg = "la demande a été prise en compte. Un email de confirmation vous a été transmis"
                Else
                    Msg = "la demande n'a pas pu aboutir. "
                End If
            Else
                Msg = "La modification n'a pas pu aboutir."
            End If
            EtiMessage.Text = Msg
        End If
    End Sub

    Private Sub VerifierCookie()
        If Request.Cookies("MyVirtualia") Is Nothing Then
            Exit Sub
        End If
        If Me.ViewState(WsNomStateValid) Is Nothing Then
            Exit Sub
        End If
        If WsRhFonction Is Nothing Then
            WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
        End If
        Dim Vcookie As System.Web.HttpCookie = Request.Cookies.Get("MyVirtualia")
        Dim Ctl_Cache As Virtualia.Net.VCaches.CacheIdentification
        WsCacheValid = CType(Me.ViewState(WsNomStateValid), List(Of String))

        Ctl_Cache = New Virtualia.Net.VCaches.CacheIdentification
        Ctl_Cache.Chaine_Cookie = V_WebFonction.DecodageCookie(Vcookie.Value)
        Ctl_Cache.MotdePasseCrypte = WsRhFonction.HashSHA(WsCacheValid.Item(1))
        Vcookie.Value = V_WebFonction.EncodageCookie(Ctl_Cache.Chaine_Cookie)
        Vcookie.Expires = DateTime.Now.AddMonths(3)
        Response.Cookies.Add(Vcookie)
    End Sub

End Class