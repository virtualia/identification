﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VMenuIcones.ascx.vb" Inherits="Virtualia.Net.Controles_VMenuIcones" %>

<%@ Register src="VCoupleIconeEti.ascx" tagname="VIcone" tagprefix="Virtualia" %>

<asp:Panel ID="CadreMenu" runat="server" BackColor="#0F2F30" HorizontalAlign="Center" Width="1146px" 
                BorderColor="#5E9598" BorderStyle="Solid" BorderWidth="2px" Height="500px" ScrollBars="Auto"
                Style="margin-top: 0px; text-align: left; ">
       <asp:Table ID="CadreChoix" runat="server" HorizontalAlign="Left">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table ID="CadreTuile01" runat="server" HorizontalAlign="Left">
                     <asp:TableRow>
                         <asp:TableCell Width="150px" Height="150px">
                           <Virtualia:VIcone ID="Cmd01" runat="server" IconeBackColor="#1C5151" IconeURL="~/Images/General/VirImag_BleuVert.jpg"
                                             EtiText="Mon dossier" IconeTooltip="Accés direct au self Virtualia" 
                                             ParametreURL="V3" VIndex="1" />
                        </asp:TableCell>
                     </asp:TableRow>
                     <asp:TableRow>
                        <asp:TableCell Width="150px" Height="150px">
                           <Virtualia:VIcone ID="Cmd02" runat="server" IconeBackColor="#1C5151" IconeURL="~/Images/General/VirImag_BleuVertGris.jpg"
                                             EtiText="Annuaire et répertoire" IconeTooltip="Annuaire et répertoire en ligne" 
                                             ParametreURL="V3" VIndex="2" />
                        </asp:TableCell>
                     </asp:TableRow>
                     <asp:TableRow>
                        <asp:TableCell Width="150px" Height="150px">
                           <Virtualia:VIcone ID="Cmd03" runat="server" IconeBackColor="#1C5151" IconeURL="~/Images/General/VirImag_Bleu.jpg"
                                             EtiText="Suivi des informations personnelles" IconeTooltip="Informations personnelles (Etat-civil, Enfants, diplômes etc...)"
                                             ParametreURL="ID" VIndex="3"/>
                        </asp:TableCell>
                     </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
            <asp:TableCell>
                <asp:Table ID="CadreTuile02" runat="server" HorizontalAlign="Left">
                     <asp:TableRow>
                        <asp:TableCell Width="150px" Height="150px">
                            <Virtualia:VIcone ID="Cmd04" runat="server" IconeBackColor="#0E5F5C" IconeURL="~/Images/General/VirImag_BleuGris.jpg"
                                             EtiText="Gestion des situations administratives et contractuelles" IconeTooltip="Suivi des informations organisant le lien juridique" 
                                             ParametreURL="ID" VIndex="4" />
                        </asp:TableCell>
                     </asp:TableRow>
                     <asp:TableRow>
                       <asp:TableCell Width="150px" Height="150px">
                            <Virtualia:VIcone ID="Cmd05" runat="server" IconeBackColor="#0E5F5C" IconeURL="~/Images/General/VirImag_Violet.jpg"
                                             EtiText="Gestion du temps de travail, des congés et des absences" IconeTooltip="Gestion des plannings, du badgeage, des congés et autorisations d'absence"
                                             ParametreURL="ID" VIndex="5" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="150px" Height="150px">
                            <Virtualia:VIcone ID="Cmd06" runat="server" IconeBackColor="#0E5F5C" IconeURL="~/Images/General/VirImag_VioletGris.jpg"
                                             EtiText="Gestion de la formation" IconeTooltip="Gestion de la formation"
                                             ParametreURL="ID" VIndex="6" />
                        </asp:TableCell>
                     </asp:TableRow>
                </asp:Table>
           </asp:TableCell>
           <asp:TableCell>
               <asp:Table ID="CadreTuile03" runat="server" HorizontalAlign="Left">
                     <asp:TableRow>
                        <asp:TableCell Width="150px" Height="150px">
                           <Virtualia:VIcone ID="Cmd07" runat="server" IconeBackColor="#137A76" IconeURL="~/Images/General/VirImag_Mauve.jpg"
                                             EtiText="Gestion des carrières" IconeTooltip=""
                                             ParametreURL="ID" VIndex="7" />
                        </asp:TableCell>
                    </asp:TableRow>
                     <asp:TableRow>
                        <asp:TableCell Width="150px" Height="150px">
                            <Virtualia:VIcone ID="Cmd08" runat="server" IconeBackColor="#137A76" IconeURL="~/Images/General/VirImag_MauveGris.jpg"
                                             EtiText="Suivi des Affectations fonctionnelles" IconeTooltip="Gestion des annuaires"
                                             ParametreURL="ID" VIndex="8" />
                        </asp:TableCell>
                   </asp:TableRow>
                   <asp:TableRow>
                       <asp:TableCell Width="150px" Height="150px">
                            <Virtualia:VIcone ID="Cmd09" runat="server" IconeBackColor="#137A76" IconeURL="~/Images/General/VirImag_Rose.jpg"
                                             EtiText="Entretiens professionnels" IconeTooltip="Gestion des entretiens professionnels"
                                             ParametreURL="ID" VIndex="9" />
                        </asp:TableCell>
                    </asp:TableRow>
               </asp:Table>
           </asp:TableCell>
           <asp:TableCell>
               <asp:Table ID="CadreTuile04" runat="server" HorizontalAlign="Left">
                     <asp:TableRow>
                        <asp:TableCell Width="150px" Height="150px">
                            <Virtualia:VIcone ID="Cmd10" runat="server" IconeBackColor="#19968D" IconeURL="~/Images/General/VirImag_RoseGris.jpg"
                                             EtiText="ETP et dépenses de personnels" IconeTooltip="Suivi des effectifs en termes d'ETP et  de dépenses de personnel"
                                             ParametreURL="ID" VIndex="10" ParametreFRM="Etp" />
                        </asp:TableCell>
                     </asp:TableRow>
                     <asp:TableRow>
                        <asp:TableCell Width="150px" Height="150px">
                           <Virtualia:VIcone ID="Cmd11" runat="server" IconeBackColor="#19968D" IconeURL="~/Images/General/VirImag_Rouge.jpg"
                                             EtiText="Module de préparation de la paie" IconeTooltip="Module de préparation de la paie"
                                             ParametreURL="ID" VIndex="11" />
                        </asp:TableCell>
                    </asp:TableRow>
                     <asp:TableRow>
                        <asp:TableCell Width="150px" Height="150px">
                            <Virtualia:VIcone ID="Cmd12" runat="server" IconeBackColor="#19968D" IconeURL="~/Images/General/VirImag_RougeGris.jpg"
                                             EtiText="Gestion du système de référence" IconeTooltip="Administration et gestion des référentiels"
                                             ParametreURL="ID" VIndex="12" />
                        </asp:TableCell>
                   </asp:TableRow>
               </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
               <asp:Table ID="CadreTuile05" runat="server" HorizontalAlign="Left">
                   <asp:TableRow>
                       <asp:TableCell Width="150px" Height="150px">
                            <Virtualia:VIcone ID="Cmd13" runat="server" IconeBackColor="#1EB3A8" IconeURL="~/Images/General/VirImag_Orange.jpg"
                                             EtiText="Module d'administration" IconeTooltip=""
                                             ParametreURL="ID" VIndex="13" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="150px" Height="150px">
                            <Virtualia:VIcone ID="Cmd14" runat="server" IconeBackColor="#1EB3A8" IconeURL="~/Images/General/VirImag_OrangeGris.jpg"
                                             EtiText="Déconnexion" IconeTooltip="Se déconnecter de Virtualia.Net" NavigateURL="Connexion/FrmDeconnexion.aspx"
                                             ParametreURL="" VIndex="14" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="150px" Height="150px">
                           <Virtualia:VIcone ID="Cmd15" runat="server" IconeBackColor="#1EB3A8" IconeURL="~/Images/General/VirImag_Marron.jpg"
                                             EtiText="" IconeTooltip="" SiVisible="false"
                                             ParametreURL="ID" VIndex="15" />
                        </asp:TableCell>
                    </asp:TableRow>
               </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
               <asp:Table ID="CadreTuile06" runat="server" HorizontalAlign="Left">
                   <asp:TableRow>
                        <asp:TableCell Width="150px" Height="150px">
                            <Virtualia:VIcone ID="Cmd16" runat="server" IconeBackColor="#7EC8BE" IconeURL="~/Images/General/VirImag_MarronGris.jpg"
                                             EtiText="" IconeTooltip="" SiVisible="false"
                                             ParametreURL="ID" VIndex="16" />
                        </asp:TableCell>
                   </asp:TableRow>
                   <asp:TableRow>
                       <asp:TableCell Width="150px" Height="150px">
                            <Virtualia:VIcone ID="Cmd17" runat="server" IconeBackColor="#7EC8BE" IconeURL="~/Images/General/VirImag_Or.jpg"
                                             EtiText="" IconeTooltip="" SiVisible="false"
                                             ParametreURL="ID" VIndex="17" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="150px" Height="150px">
                            <Virtualia:VIcone ID="Cmd18" runat="server" IconeBackColor="#7EC8BE" IconeURL="~/Images/General/VirImag_OrGris.jpg"
                                             EtiText="" IconeTooltip="" SiVisible="false"
                                             ParametreURL="ID" VIndex="18" />
                        </asp:TableCell>
                    </asp:TableRow>
               </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
               <asp:Table ID="CadreTuile07" runat="server" HorizontalAlign="Left">
                    <asp:TableRow>
                        <asp:TableCell Width="150px" Height="150px">
                           <Virtualia:VIcone ID="Cmd19" runat="server" IconeBackColor="#1EB3A8" IconeURL="~/Images/General/VirImag_JauneVert.jpg"
                                             EtiText="" IconeTooltip="" SiVisible="false"
                                             ParametreURL="ID" VIndex="19" />
                        </asp:TableCell>
                    </asp:TableRow>
                     <asp:TableRow>
                        <asp:TableCell Width="150px" Height="150px">
                            <Virtualia:VIcone ID="Cmd20" runat="server" IconeBackColor="#1EB3A8" IconeURL="~/Images/General/VirImag_JauneVertGris.jpg"
                                             EtiText="" IconeTooltip="" SiVisible="false"
                                             ParametreURL="ID" VIndex="20" />
                        </asp:TableCell>
                   </asp:TableRow>
                   <asp:TableRow>
                       <asp:TableCell Width="150px" Height="150px">
                            <Virtualia:VIcone ID="Cmd21" runat="server" IconeBackColor="#1EB3A8" IconeURL="~/Images/General/VirImag_Bronze.jpg"
                                             EtiText="" IconeTooltip="" SiVisible="false"
                                             ParametreURL="ID" VIndex="21" />
                        </asp:TableCell>
                    </asp:TableRow>
               </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
               <asp:Table ID="CadreTuile08" runat="server" HorizontalAlign="Left">
                    <asp:TableRow>
                        <asp:TableCell Width="150px" Height="150px">
                           <Virtualia:VIcone ID="Cmd22" runat="server" IconeBackColor="#19968D" IconeURL="~/Images/General/VirImag_BronzeGris.jpg"
                                             EtiText="" IconeTooltip="" SiVisible="false"
                                             ParametreURL="ID" VIndex="22" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="150px" Height="150px">
                            <Virtualia:VIcone ID="Cmd23" runat="server" IconeBackColor="#19968D" IconeURL="~/Images/General/VirImag_Vert.jpg"
                                             EtiText="" IconeTooltip="" SiVisible="false"
                                             ParametreURL="ID" VIndex="23" />
                        </asp:TableCell>
                    </asp:TableRow>
                     <asp:TableRow>
                        <asp:TableCell Width="150px" Height="150px">
                            <Virtualia:VIcone ID="Cmd24" runat="server" IconeBackColor="#19968D" IconeURL="~/Images/General/VirImag_VertGris.jpg"
                                             EtiText="" IconeTooltip="" SiVisible="false"
                                             ParametreURL="ID" VIndex="24" />
                        </asp:TableCell>
                   </asp:TableRow>
               </asp:Table>
            </asp:TableCell>   
        </asp:TableRow>
    </asp:Table>
 </asp:Panel>
