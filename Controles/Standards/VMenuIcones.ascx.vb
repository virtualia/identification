﻿Option Strict On
Option Explicit On
Option Compare Text
Public Class Controles_VMenuIcones
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Click_EventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.AppelChoixEventArgs)
    Public Event CmdIcone_Click As Click_EventHandler
    Private WebFct As Virtualia.Net.WebFonctions

    Private ReadOnly Property V_WebFonction As Virtualia.Net.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Public Property SiIconeEnabled(ByVal Index As Integer) As Boolean
        Get
            Dim Ctl As Control
            Dim VirControle As Virtualia.Net.Controles_VCoupleIconeEti
            Dim IndiceI As Integer

            IndiceI = 0
            Do
                Ctl = V_WebFonction.VirWebControle(Me.CadreChoix, "Cmd", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                VirControle = CType(Ctl, Virtualia.Net.Controles_VCoupleIconeEti)
                If VirControle.VIndex = Index Then
                    Return VirControle.SiEnable
                End If
                IndiceI += 1
            Loop
            Return False
        End Get
        Set(value As Boolean)
            Dim Ctl As Control
            Dim VirControle As Virtualia.Net.Controles_VCoupleIconeEti
            Dim IndiceI As Integer

            IndiceI = 0
            Do
                Ctl = V_WebFonction.VirWebControle(Me.CadreChoix, "Cmd", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                VirControle = CType(Ctl, Virtualia.Net.Controles_VCoupleIconeEti)
                If VirControle.VIndex = Index Then
                    VirControle.SiEnable = value
                End If
                IndiceI += 1
            Loop
        End Set
    End Property

    Public Property SiIconeVisible(ByVal Index As Integer) As Boolean
        Get
            Dim Ctl As Control
            Dim VirControle As Virtualia.Net.Controles_VCoupleIconeEti
            Dim IndiceI As Integer

            IndiceI = 0
            Do
                Ctl = V_WebFonction.VirWebControle(Me.CadreChoix, "Cmd", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                VirControle = CType(Ctl, Virtualia.Net.Controles_VCoupleIconeEti)
                If VirControle.VIndex = Index Then
                    Return VirControle.Visible
                End If
                IndiceI += 1
            Loop
            Return False
        End Get
        Set(value As Boolean)
            Dim Ctl As Control
            Dim VirControle As Virtualia.Net.Controles_VCoupleIconeEti
            Dim IndiceI As Integer

            IndiceI = 0
            Do
                Ctl = V_WebFonction.VirWebControle(Me.CadreChoix, "Cmd", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                VirControle = CType(Ctl, Virtualia.Net.Controles_VCoupleIconeEti)
                If VirControle.VIndex = Index Then
                    VirControle.Visible = value
                End If
                IndiceI += 1
            Loop
        End Set
    End Property

    Protected Overridable Sub VIcone_Click(ByVal e As Virtualia.Systeme.Evenements.AppelChoixEventArgs)
        RaiseEvent CmdIcone_Click(Me, e)
    End Sub

    Public ReadOnly Property V_NoSession As String
        Get
            If Session.Item("IDVirtualia") IsNot Nothing Then
                Return Session.Item("IDVirtualia").ToString
            Else
                Return ""
            End If
        End Get
    End Property

    Private Sub CmdOKClick(sender As Object, e As Systeme.Evenements.AppelChoixEventArgs) Handles Cmd01.CmdOK_Click,
        Cmd02.CmdOK_Click, Cmd03.CmdOK_Click, Cmd04.CmdOK_Click, Cmd05.CmdOK_Click, Cmd06.CmdOK_Click, Cmd07.CmdOK_Click,
        Cmd08.CmdOK_Click, Cmd09.CmdOK_Click, Cmd10.CmdOK_Click, Cmd11.CmdOK_Click, Cmd12.CmdOK_Click, Cmd13.CmdOK_Click,
        Cmd14.CmdOK_Click, Cmd15.CmdOK_Click, Cmd16.CmdOK_Click, Cmd17.CmdOK_Click, Cmd18.CmdOK_Click, Cmd19.CmdOK_Click,
        Cmd20.CmdOK_Click, Cmd21.CmdOK_Click, Cmd22.CmdOK_Click, Cmd23.CmdOK_Click, Cmd24.CmdOK_Click

        If e.Valeurs Is Nothing Then
            Exit Sub
        End If
        Dim TabValeurs As ArrayList = e.Valeurs
        Dim UrlWse As String = ""
        Select Case TabValeurs(2).ToString
            Case ""
                If TabValeurs(1) Is Nothing Then
                    Exit Sub
                End If
                If TabValeurs(1).ToString = "" Then
                    Exit Sub
                End If
                Server.Transfer("~/Fenetres/" & TabValeurs(1).ToString)
            Case "ID", "V3"
                UrlWse = V_WebFonction.PointeurGlobal.UrlDestination(V_NoSession, CInt(TabValeurs(0)), TabValeurs(3).ToString)
        End Select
        ''**** AKR pour test 
        If CInt(TabValeurs(0)) = 1 Then
            Dim virsession = V_WebFonction.PointeurGlobal.VirMdp(V_NoSession)

            'UrlWse &= "?Nom=" & virsession.Nom & "&Prenom=" & virsession.Prenom & "&Mdp=" & virsession.Mdp
            UrlWse &= "?Nom=" & virsession.Nom.ToLower & "&Prenom=" & virsession.Prenom.ToLower & "&Mdp=" & virsession.Mdp

            'UrlWse &= "?Nom=AIX-EN-PROVENCE&Prenom=Esther&Mdp=mb"
            Response.Redirect(UrlWse, True)
        End If
        ''****
        If IsNumeric(TabValeurs(0)) = False Then
            Exit Sub
        End If
        If Session.Item("NumeroLogo") IsNot Nothing Then
            Session.Remove("NumeroLogo")
        End If
        Session.Add("NumeroLogo", CInt(TabValeurs(0)))

        Response.Redirect("~/Fenetres/FrmAttente.aspx?IDVirtualia=" & Server.HtmlEncode(V_NoSession) &
                          "&Index=" & TabValeurs(0).ToString & "&Lien=" & UrlWse, True)
    End Sub
End Class