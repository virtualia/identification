﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VCoupleIconeEti.ascx.vb" ClassName="VCoupleIconeEti"
    EnableViewState="true" Inherits="Virtualia.Net.Controles_VCoupleIconeEti" %>

<asp:Table ID="VIcone" runat="server" CellPadding="1" CellSpacing="0" Width="150px" Height="150px" 
            style="margin-top:2px" BackColor="Window">
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" Width ="150px" BorderStyle ="None">
            <asp:ImageButton ID="CmdChoix" runat="server"  
                    Width="64px" Height="64px" BackColor="Transparent" BorderStyle="None">
            </asp:ImageButton>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="Etiquette" runat="server" Height="28px" Width="135px"
                    BackColor="Transparent" BorderColor="#B0E0D7"  BorderStyle="None"
                    ForeColor="White" Font-Italic="true" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="X-Small"
                     style="margin-left: 10px;text-align: left" >
            </asp:Label>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
