﻿Public Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Application.Item("CheminRelatif") Is Nothing Then
            If Request.ApplicationPath <> "/" Then
                Application.Add("CheminRelatif", Request.ApplicationPath & "/")
            Else
                Application.Add("CheminRelatif", "/")
            End If
        End If
        Dim Chaine As New System.Text.StringBuilder
        Chaine.Append("Nom Cnx Anonyme = " & Request.AnonymousID & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("Nom Cnx Windows = " & Request.LogonUserIdentity.Name & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("Si Anonyme = " & Request.LogonUserIdentity.IsAnonymous.ToString & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("Si Authentifié = " & Request.LogonUserIdentity.IsAuthenticated.ToString & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("Browser = " & Request.Browser.Id & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("Browser N° Version= " & Request.Browser.Version & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("Adresse IP = " & Request.UserHostAddress & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("Machine cliente = " & Request.UserHostName & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("Chemin Application = " & Request.PhysicalApplicationPath & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("Chemin Fichiers = " & Request.PhysicalPath & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("URL Racine = " & Request.ApplicationPath & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("URL Absolue = " & Request.Url.ToString & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("URL Relative = " & Request.RawUrl.ToString & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("URL Repertoire virtuel = " & Request.FilePath & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("URL Courante = " & Request.CurrentExecutionFilePath & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("Méthode HTTP = " & Request.HttpMethod & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("Si Https = " & Request.IsSecureConnection.ToString & Strings.Chr(13) & Strings.Chr(10))
        Chaine.Append("")

        'Page.ClientScript.RegisterStartupScript(GetType(Page), "closePage", "<script type='text/JavaScript'>window.open('', '_self', '');window.close();</script>")
        Page.ClientScript.RegisterStartupScript(GetType(Page), "closePage", "<script type='text/JavaScript'>window.open('', '_parent', '');window.close();</script>")
        '*** Pour Mozilla
        'To change the default setting :
        '1.type"about:config " in your firefox address bar and enter;
        '2.make sure your "dom.allow_scripts_to_close_windows" is true
        '3.make sure your "Dom.disable_window_open_feature.location" is false
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete
        Dim Parametre As String = ""
        Dim ModeCnx As String = ""
        Dim NomCnx As String
        Dim WebFct As Virtualia.Net.WebFonctions
        Dim NomMachine As String

        Parametre = Server.HtmlDecode(Request.QueryString("Param"))
        ModeCnx = Server.HtmlDecode(Request.QueryString("ModeCnx"))

        WebFct = New Virtualia.Net.WebFonctions(Me, 1)

        Try
            NomCnx = WebFct.PointeurGlobal.IDUtilisateur(Session.SessionID)
        Catch ex As Exception
            NomCnx = ""
        End Try
        If NomCnx <> "" Then
            If Parametre = "EXIT" Then
                Response.Redirect("~/Fenetres/Connexion/FrmDeconnexion.aspx?IDVirtualia=" & Server.HtmlEncode(Session.SessionID))
            Else
                Response.Redirect("~/Fenetres/Menus/FrmMenuPrincipal.aspx?IDVirtualia=" & Server.HtmlEncode(Session.SessionID))
            End If
            Exit Sub
        End If

        If ModeCnx IsNot Nothing Then
            Select Case ModeCnx
                Case "V3", "V4"
                    WebFct.PointeurGlobal.VirModeConnexion = ModeCnx
            End Select
        End If
        ''**** AKR Pour test
        'WebFct.PointeurGlobal.VirModeConnexion = "VI"
        ''****
        Try
            NomMachine = System.Net.Dns.GetHostEntry(Request.UserHostAddress).HostName
        Catch ex As Exception
            NomMachine = Server.HtmlDecode(Request.UserHostName)
        End Try
        Session.Add("Machine", NomMachine)
        Session.Add("IP", Server.HtmlEncode(Request.UserHostAddress))
        Session.Add("Identite", Server.HtmlEncode(Request.LogonUserIdentity.Name))

        If System.Configuration.ConfigurationManager.AppSettings("SiAutonome") = "Oui" Then
            VirWebResponse.Redirect("~/Fenetres/Connexion/FrmConnexion.aspx", "_blank", _
        "left=0px,top=0px,width=1200px,height=950px,toolbar=no,menubar=no,location=no,resizable=yes,scrollbars=yes")
        Else
            Response.Redirect("~/Fenetres/Connexion/FrmConnexion.aspx")
        End If

    End Sub

End Class
