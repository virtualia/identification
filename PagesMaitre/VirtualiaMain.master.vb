﻿Public Class VirtualiaMain
    Inherits System.Web.UI.MasterPage
    Private WebFct As Virtualia.Net.WebFonctions

    Public WriteOnly Property SiFRmCnx As Boolean
        Set(value As Boolean)
            InfoGenMaster.Visible = False
            EtiTitreL1.Text = ""
        End Set
    End Property

    Public WriteOnly Property V_Logo As Integer
        Set(value As Integer)
            If Session.Item("NumeroLogo") IsNot Nothing Then
                Session.Remove("NumeroLogo")
            End If
            Session.Add("NumeroLogo", value)
            Dim Charte As New Virtualia.Systeme.Fonctions.CharteGraphique
            ImageLogo.ImageUrl = "~/Images/General/" & Charte.ImageTuile(value)
        End Set
    End Property

    Public Property V_NoSession As String
        Get
            If Session.Item("IDVirtualia") IsNot Nothing Then
                Return Session.Item("IDVirtualia").ToString
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            If Session.Item("IDVirtualia") IsNot Nothing Then
                Session.Remove("IDVirtualia")
            End If
            Session.Add("IDVirtualia", value)
        End Set
    End Property

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        WebFct = New Virtualia.Net.WebFonctions(Me, 2)
        If Session.Item("NumeroLogo") IsNot Nothing Then
            If IsNumeric(Session.Item("NumeroLogo").ToString) Then
                V_Logo = CInt(Session.Item("NumeroLogo").ToString)
            End If
        End If
        Me.Page.MaintainScrollPositionOnPostBack = True
        EtiDateJour.Text = WebFct.ViRhDates.ClairDate(WebFct.ViRhDates.DateduJour, True)
        EtiHeure.Text = Format(TimeOfDay, "t")
        If Session.Item("IDVirtualia") IsNot Nothing Then
            EtiUtilisateur.Text = WebFct.PointeurGlobal.NomPrenomUtilisateur(Session.Item("IDVirtualia").ToString)
        End If

    End Sub

    Private Sub HorlogeVirtuelle_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles HorlogeVirtuelle.Tick
        EtiHeure.Text = Format(TimeOfDay, "t")
    End Sub

End Class