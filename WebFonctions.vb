﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.MetaModele

Public Class WebFonctions
    Private WsRhFonction As Virtualia.Systeme.Fonctions.Generales
    Private WsRhDates As Virtualia.Systeme.Fonctions.CalculDates
    Private WsCharte As Virtualia.Systeme.Fonctions.CharteGraphique
    Private WsTypeControle As Integer = 0 'UserControl, 1 Page, 2 MasterPage
    Private WsHostUC As System.Web.UI.UserControl
    Private WsHostP As System.Web.UI.Page
    Private WsHostMP As System.Web.UI.MasterPage
    Private Const TypeUC As Integer = 0
    Private Const TypeP As Integer = 1
    Private Const TypeMP As Integer = 2
    '
    Private WsAppObjetGlobal As Virtualia.Net.Session.ObjetGlobal
    Private WsNosession As String

    Public ReadOnly Property PointeurGlobal As Virtualia.Net.Session.ObjetGlobal
        Get
            Return WsAppObjetGlobal
        End Get
    End Property

    Public ReadOnly Property ViRhDates As Virtualia.Systeme.Fonctions.CalculDates
        Get
            Return WsRhDates
        End Get
    End Property

    Public ReadOnly Property ViRhFonction As Virtualia.Systeme.Fonctions.Generales
        Get
            Return WsRhFonction
        End Get
    End Property

    Public ReadOnly Property Evt_MessageInformatif(ByVal NumObjet As Integer, ByVal TitreMsg As String, ByVal MsgInfos As ArrayList, Optional ByVal Ide As Integer = 0) As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        Get
            If MsgInfos Is Nothing Then
                Return Nothing
            End If
            Dim Msg As String = ""
            Dim I As Integer

            For I = 0 To MsgInfos.Count - 1
                Msg &= MsgInfos(I).ToString & Strings.Chr(13) & Strings.Chr(10)
            Next I

            Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
            Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("MajDossier", NumObjet, Ide.ToString, "OK", TitreMsg, Msg)
            Return Evenement
        End Get
    End Property

    Public ReadOnly Property Evt_MessageBloquant(ByVal NumObjet As Integer, ByVal TitreMsg As String, ByVal MsgErreurs As ArrayList) As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        Get
            If MsgErreurs Is Nothing Then
                Return Nothing
            End If
            Dim Msg As String = ""
            Dim I As Integer

            For I = 0 To MsgErreurs.Count - 1
                Msg &= " Anomalie bloquante : " & MsgErreurs(I).ToString & Strings.Chr(13) & Strings.Chr(10)
            Next I

            Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
            Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("MajDossier", NumObjet, "", "KO", TitreMsg, Msg)
            Return Evenement
        End Get
    End Property

    Public ReadOnly Property EncodageCookie(ByVal Valeur As String) As String
        Get
            Dim TableauByte() As Byte
            TableauByte = System.Text.Encoding.UTF8.GetBytes(Valeur)
            Return BitConverter.ToString(TableauByte)
        End Get
    End Property

    Public ReadOnly Property DecodageCookie(ByVal Valeur As String) As String
        Get
            Dim I As Integer
            Dim Tableaudata() As String = Strings.Split(Valeur, "-")
            Dim TableauByte() As Byte
            ReDim TableauByte(Tableaudata.Length - 1)
            Try
                For I = 0 To TableauByte.Count - 1
                    TableauByte(I) = Convert.ToByte(Tableaudata(I), 16)
                Next I
                Return System.Text.Encoding.UTF8.GetString(TableauByte)
            Catch ex As Exception
                Return Valeur
            End Try
        End Get
    End Property

    Public Function InverseGEST(ByVal NatureDonnee As String, ByVal FormatDonnee As String, ByVal Longueur As Integer, ByVal Valeur As String) As String
        Dim Chaine As String = Valeur
        Dim FicheVir As New Virtualia.Systeme.MetaModele.VIR_FICHE
        Dim ZCalc As Double

        If Valeur.Trim = "" Then
            Return ""
        End If
        Select Case FormatDonnee
            Case Is = "EntierOuBL"
                If Valeur.Trim <> "" Then
                    ZCalc = Val(Valeur)
                    Chaine = VIR_FICHE.F_FormatNumerique(ZCalc.ToString)
                Else
                    Chaine = VIR_FICHE.F_FormatAlpha(Valeur.ToUpper, Longueur)
                End If
                Return Chaine
        End Select

        Select Case NatureDonnee
            Case Is = "Alpha"
                Chaine = VIR_FICHE.F_FormatAlpha(Valeur.ToUpper, Longueur)
                Chaine = WsRhFonction.ChaineSansAccent(Chaine, True)
            Case Is = "Date"
                Select Case FormatDonnee
                    Case Is = "AAAAMM"
                        Chaine = WsRhDates.DateSaisieVerifiee("01" & Strings.Right(Valeur, 2) & Strings.Left(Valeur, 4))
                    Case Is = "MMAAAA"
                        Chaine = WsRhDates.DateSaisieVerifiee("01" & Valeur)
                    Case Is = "JJMMAAAA"
                        Chaine = WsRhDates.DateSaisieVerifiee(Valeur)
                    Case Else
                        Chaine = WsRhDates.DateSaisieVerifiee(Valeur)
                End Select
            Case Is = "Numerique"
                Dim NbDecimales As Integer = 0
                Select Case FormatDonnee
                    Case Is = "Decimal1"
                        NbDecimales = 1
                        ZCalc = Val(Valeur) / 10
                    Case Is = "Decimal2"
                        NbDecimales = 2
                        ZCalc = Val(Valeur) / 100
                    Case Is = "EntierBL"
                        ZCalc = Val(Valeur)
                        If ZCalc = 0 Then
                            Return ""
                        End If
                    Case Else
                        ZCalc = Val(Valeur)
                End Select
                Chaine = VIR_FICHE.F_FormatNumerique(ZCalc.ToString, NbDecimales)
        End Select
        Return Chaine
    End Function

    Public Function LostFocusStd(ByVal NatureDonnee As String, ByVal FormatDonnee As String, ByVal Longueur As Integer, ByVal Valeur As String) As String
        Dim Chaine As String = Valeur
        Dim FicheVir As New Virtualia.Systeme.MetaModele.VIR_FICHE

        Select Case NatureDonnee
            Case Is = "Alpha"
                Chaine = VIR_FICHE.F_FormatAlpha(Valeur.ToUpper, Longueur)
                Chaine = WsRhFonction.ChaineSansAccent(Chaine, True)
            Case Is = "Date"
                Chaine = WsRhDates.DateSaisieVerifiee(Valeur)
            Case Is = "Numerique"
                Dim NbDecimales As Integer = 0
                Select Case FormatDonnee
                    Case Is = "Decimal1"
                        NbDecimales = 1
                    Case Is = "Decimal2"
                        NbDecimales = 2
                End Select
                Chaine = VIR_FICHE.F_FormatNumerique(Valeur, NbDecimales)
            Case Is = "Table"
                If Valeur = "Blanc" Then
                    Return ""
                End If
        End Select
        Return Chaine
    End Function

    Public Function InverseBASEPAY(ByVal NatureDonnee As String, ByVal Valeur As String) As String
        Dim Chaine As String = Valeur
        Select Case NatureDonnee
            Case Is = "Date"
                If Valeur.Length = 8 Then
                    Chaine = Strings.Right(Valeur, 2) & "/" & Strings.Mid(Valeur, 5, 2) & "/" & Strings.Left(Valeur, 4)
                ElseIf Valeur.Length = 6 Then
                    Chaine = Strings.Right(Valeur, 2) & "/" & Strings.Left(Valeur, 4)
                Else
                    Chaine = ""
                End If
            Case Is = "Numerique"
                If Strings.InStr(Valeur, ",") > 0 Or Strings.InStr(Valeur, ".") > 0 Then
                    If Val(WsRhFonction.VirgulePoint(Valeur)) = 0 Then
                        Chaine = ""
                    Else
                        Chaine = Strings.Format(Val(WsRhFonction.VirgulePoint(Valeur)), "#0.00")
                    End If
                Else
                    If Val(Valeur) = 0 Then
                        Chaine = ""
                    Else
                        Chaine = Val(Valeur).ToString
                    End If
                End If
        End Select
        Return Chaine
    End Function

    Private Sub InitialiserSession()
        Dim NoBd As Integer = CInt(System.Configuration.ConfigurationManager.AppSettings("NumeroDatabase"))
        Select Case WsTypeControle
            Case TypeUC
                WsAppObjetGlobal = CType(WsHostUC.Application.Item("VirGlobales"), Virtualia.Net.Session.ObjetGlobal)
                If WsHostUC.Session.Item("IDVirtualia") IsNot Nothing Then
                    WsNosession = WsHostUC.Session.Item("IDVirtualia").ToString
                End If
            Case TypeP
                WsAppObjetGlobal = CType(WsHostP.Application.Item("VirGlobales"), Virtualia.Net.Session.ObjetGlobal)
                If WsHostP.Session.Item("IDVirtualia") IsNot Nothing Then
                    WsNosession = WsHostP.Session.Item("IDVirtualia").ToString
                End If
            Case TypeMP
                WsAppObjetGlobal = CType(WsHostMP.Application.Item("VirGlobales"), Virtualia.Net.Session.ObjetGlobal)
                If WsHostMP.Session.Item("IDVirtualia") IsNot Nothing Then
                    WsNosession = WsHostMP.Session.Item("IDVirtualia").ToString
                End If
        End Select
        If WsAppObjetGlobal Is Nothing Then
            Dim VirObjetGlobal As Virtualia.Net.Session.ObjetGlobal
            Try
                VirObjetGlobal = New Virtualia.Net.Session.ObjetGlobal(System.Configuration.ConfigurationManager.AppSettings("NomUtilisateur"), NoBd)
                Select Case WsTypeControle
                    Case TypeUC
                        WsHostUC.Application.Add("VirGlobales", VirObjetGlobal)
                    Case TypeP
                        WsHostP.Application.Add("VirGlobales", VirObjetGlobal)
                    Case TypeMP
                        WsHostMP.Application.Add("VirGlobales", VirObjetGlobal)
                End Select
            Catch ex As Exception
                Exit Sub
            End Try
        End If
    End Sub

    Public ReadOnly Property VirWebControle(ByVal CadreInfo As Control, ByVal Prefixe As String, ByVal Index As Integer) As Control
        Get
            Dim Cpt As Integer = -1
            Dim IndiceI As Integer
            Dim IndiceN1 As Integer
            Dim IndiceN2 As Integer
            Dim IndiceN3 As Integer
            Dim IndiceN4 As Integer
            Dim IndiceN5 As Integer
            Dim IndiceN6 As Integer
            Dim IndiceN7 As Integer
            Dim IndiceN8 As Integer
            Dim N0 As Control
            Dim N1 As Control
            Dim N2 As Control
            Dim N3 As Control
            Dim N4 As Control
            Dim N5 As Control
            Dim N6 As Control
            Dim N7 As Control
            Dim N8 As Control
            Dim Lg As Integer = Prefixe.Length

            For IndiceI = 0 To CadreInfo.Controls.Count - 1
                N0 = CadreInfo.Controls.Item(IndiceI)
                Select Case Strings.Left(N0.ID, Lg)
                    Case Is = Prefixe
                        Cpt += 1
                        If Cpt = Index Then
                            Return N0
                        End If
                    Case Else
                        For IndiceN1 = 0 To N0.Controls.Count - 1
                            N1 = N0.Controls.Item(IndiceN1)
                            Select Case Strings.Left(N1.ID, Lg)
                                Case Is = Prefixe
                                    Cpt += 1
                                    If Cpt = Index Then
                                        Return N1
                                    End If
                                Case Else
                                    For IndiceN2 = 0 To N1.Controls.Count - 1
                                        N2 = N1.Controls.Item(IndiceN2)
                                        Select Case Strings.Left(N2.ID, Lg)
                                            Case Is = Prefixe
                                                Cpt += 1
                                                If Cpt = Index Then
                                                    Return N2
                                                End If
                                            Case Else
                                                For IndiceN3 = 0 To N2.Controls.Count - 1
                                                    N3 = N2.Controls.Item(IndiceN3)
                                                    Select Case Strings.Left(N3.ID, Lg)
                                                        Case Is = Prefixe
                                                            Cpt += 1
                                                            If Cpt = Index Then
                                                                Return N3
                                                            End If
                                                        Case Else
                                                            For IndiceN4 = 0 To N3.Controls.Count - 1
                                                                N4 = N3.Controls.Item(IndiceN4)
                                                                Select Case Strings.Left(N4.ID, Lg)
                                                                    Case Is = Prefixe
                                                                        Cpt += 1
                                                                        If Cpt = Index Then
                                                                            Return N4
                                                                        End If
                                                                    Case Else
                                                                        For IndiceN5 = 0 To N4.Controls.Count - 1
                                                                            N5 = N4.Controls.Item(IndiceN5)
                                                                            Select Case Strings.Left(N5.ID, Lg)
                                                                                Case Is = Prefixe
                                                                                    Cpt += 1
                                                                                    If Cpt = Index Then
                                                                                        Return N5
                                                                                    End If
                                                                                Case Else
                                                                                    For IndiceN6 = 0 To N5.Controls.Count - 1
                                                                                        N6 = N5.Controls.Item(IndiceN6)
                                                                                        Select Case Strings.Left(N6.ID, Lg)
                                                                                            Case Is = Prefixe
                                                                                                Cpt += 1
                                                                                                If Cpt = Index Then
                                                                                                    Return N6
                                                                                                End If
                                                                                            Case Else
                                                                                                For IndiceN7 = 0 To N6.Controls.Count - 1
                                                                                                    N7 = N6.Controls.Item(IndiceN7)
                                                                                                    Select Case Strings.Left(N7.ID, Lg)
                                                                                                        Case Is = Prefixe
                                                                                                            Cpt += 1
                                                                                                            If Cpt = Index Then
                                                                                                                Return N7
                                                                                                            End If
                                                                                                        Case Else
                                                                                                            For IndiceN8 = 0 To N7.Controls.Count - 1
                                                                                                                N8 = N7.Controls.Item(IndiceN8)
                                                                                                                Select Case Strings.Left(N8.ID, Lg)
                                                                                                                    Case Is = Prefixe
                                                                                                                        Cpt += 1
                                                                                                                        If Cpt = Index Then
                                                                                                                            Return N8
                                                                                                                        End If
                                                                                                                End Select
                                                                                                            Next IndiceN8
                                                                                                    End Select
                                                                                                Next IndiceN7
                                                                                        End Select
                                                                                    Next IndiceN6
                                                                            End Select
                                                                        Next IndiceN5
                                                                End Select
                                                            Next IndiceN4
                                                    End Select
                                                Next IndiceN3
                                        End Select
                                    Next IndiceN2
                            End Select
                        Next IndiceN1
                End Select
            Next IndiceI
            Return Nothing
        End Get
    End Property

    Public ReadOnly Property CouleurMaj() As System.Drawing.Color
        Get
            Return ConvertCouleur("DEFAD7")
        End Get
    End Property

    Public Function ConvertCouleur(ByVal Valeur As String) As System.Drawing.Color
        Dim R As Integer
        Dim G As Integer
        Dim B As Integer
        Select Case Valeur.Length
            Case Is = 6
                R = CInt(Val("&H" & Strings.Left(Valeur, 2) & "&"))
                G = CInt(Val("&H" & Strings.Mid(Valeur, 3, 2) & "&"))
                B = CInt(Val("&H" & Strings.Right(Valeur, 2) & "&"))
                Return System.Drawing.Color.FromArgb(R, G, B)
            Case Is = 7
                R = CInt(Val("&H" & Strings.Mid(Valeur, 2, 2) & "&"))
                G = CInt(Val("&H" & Strings.Mid(Valeur, 4, 2) & "&"))
                B = CInt(Val("&H" & Strings.Right(Valeur, 2) & "&"))
                Return System.Drawing.Color.FromArgb(R, G, B)
            Case Else
                Return Drawing.Color.White
        End Select
    End Function

    Public Sub New(ByVal Host As Object, ByVal TypeControle As Integer)
        WsTypeControle = TypeControle
        Select Case WsTypeControle
            Case TypeUC
                WsHostUC = CType(Host, System.Web.UI.UserControl)
            Case TypeP
                WsHostP = CType(Host, System.Web.UI.Page)
            Case TypeMP
                WsHostMP = CType(Host, System.Web.UI.MasterPage)
        End Select
        Call InitialiserSession()
        WsRhFonction = New Virtualia.Systeme.Fonctions.Generales
        WsRhDates = New Virtualia.Systeme.Fonctions.CalculDates
    End Sub
End Class
