﻿Option Explicit On
Option Strict On
Option Compare Text
Imports Microsoft.VisualBasic
Imports VI = Virtualia.Systeme.Constantes
Namespace Session
    Public Class ObjetGlobal
        Private WsRhDates As Virtualia.Systeme.Fonctions.CalculDates
        Private WsRhFct As Virtualia.Systeme.Fonctions.Generales
        Private WsRhModele As Virtualia.Systeme.MetaModele.Donnees.ModeleRH
        Private WsInstanceSgbd As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbd

        Private WsModeConnexion As String = "V3V4" 'V4=Via New Uti gestion, "AD", V3=Via Version 3, VI=Intranet Standard
        Private WsNomUtilisateurBd As String
        Private WsNoBd As Integer
        Private WsRepertoireVirtualia As String = System.Configuration.ConfigurationManager.AppSettings("RepertoireVirtualia")
        Private WsMdp As String = ""
        Private WslstMdp As List(Of VirSessionMdp)
        Public ReadOnly Property UrlDestination(ByVal NoSession As String, ByVal Index As Integer, ByVal Param As String) As String
            Get
                If NoSession = "" Then
                    Return ""
                End If
                Dim WcfSession As Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                Dim UrlAppli As String

                WcfSession = New Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                UrlAppli = WcfSession.UrlWebApplication(NoSession, Index, Param)
                WcfSession.Close()

                Return UrlAppli
            End Get
        End Property

        Public ReadOnly Property IDUtilisateur(ByVal NoSession As String) As String
            Get
                Dim WcfSession As Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                Dim CnxUti As Virtualia.Net.ServiceSessions.SessionUtilisateurType

                WcfSession = New Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                CnxUti = WcfSession.UtilisateurVirtualia(NoSession)
                WcfSession.Close()
                If CnxUti Is Nothing Then
                    Return ""
                Else
                    Return CnxUti.ID_NomConnexion
                End If
            End Get
        End Property

        Public ReadOnly Property NomPrenomUtilisateur(ByVal NoSession As String) As String
            Get
                Dim WcfSession As Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                Dim CnxUti As Virtualia.Net.ServiceSessions.SessionUtilisateurType

                WcfSession = New Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                CnxUti = WcfSession.UtilisateurVirtualia(NoSession)
                WcfSession.Close()
                If CnxUti Is Nothing Then
                    Return ""
                Else
                    Return CnxUti.Nom_Usuel & Strings.Space(1) & CnxUti.Prenom_Usuel
                End If
            End Get
        End Property

        Public ReadOnly Property SiConnexionAutorisee(ByVal ChaineCnx As String, ByVal Identite As String, ByVal Password As String, Optional ByVal ModeCnx As String = "") As Boolean
            Get
                Dim WcfSession As Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                Dim SiOk As Boolean
                If ModeCnx = "" Then
                    ModeCnx = WsModeConnexion
                End If
                '** Password CryptageSHA
                WcfSession = New Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                SiOk = WcfSession.SiConnexionAutorisee(WsNoBd, ModeCnx, ChaineCnx.Replace("\", "_"), Identite, Password)
                WcfSession.Close()
                Return SiOk
            End Get
        End Property

        Public ReadOnly Property SiModificationPWOK(ByVal Identite As String, ByVal Password As String) As Boolean
            Get
                Dim WcfSession As Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                Dim SiOk As Boolean
                '** Password CryptageSHA

                WcfSession = New Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                SiOk = WcfSession.SiModificationPasswordOK(WsNoBd, WsModeConnexion, Identite, Password)
                WcfSession.Close()
                Return SiOk
            End Get
        End Property

        Public ReadOnly Property NouveauMotdePasse(ByVal Identite As String, ByVal Email As String) As String
            Get
                Dim WcfSession As Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                Dim NewPw As String

                WcfSession = New Virtualia.Net.ServiceSessions.VirtualiaSessionClient
                NewPw = WcfSession.NouveauPassword(WsNoBd, WsModeConnexion, Identite, Email)
                WcfSession.Close()
                Return NewPw
            End Get
        End Property

        Public ReadOnly Property EMailUtilisateur(ByVal NomCnx As String, ByVal PrenomCnx As String) As String
            Get
                Dim WcfServeur As Virtualia.Net.ServiceServeur.VirtualiaServeurClient
                Dim LstUti As List(Of ServiceServeur.UtilisateurType)
                Dim Uti As ServiceServeur.UtilisateurType

                WcfServeur = New Virtualia.Net.ServiceServeur.VirtualiaServeurClient
                LstUti = WcfServeur.ListedeTouslesUtilisateurs(WsNoBd)
                WcfServeur.Close()
                If LstUti Is Nothing Then
                    Return ""
                End If
                If PrenomCnx <> "" Then
                    Uti = LstUti.Find(Function(Recherche) Recherche.Nom = NomCnx And Recherche.Prenom = PrenomCnx)
                Else
                    Uti = LstUti.Find(Function(Recherche) Recherche.Nom = NomCnx)
                End If
                If Uti Is Nothing Then
                    Return ""
                End If
                Return ""
            End Get
        End Property

        Public Sub Deconnexion(ByVal NoSession As String)
            Dim WcfSession As Virtualia.Net.ServiceSessions.VirtualiaSessionClient
            Dim SiOk As Boolean

            WcfSession = New Virtualia.Net.ServiceSessions.VirtualiaSessionClient
            SiOk = WcfSession.Deconnexion(NoSession)
            WcfSession.Close()
        End Sub

        Public ReadOnly Property VirNomUtilisateurSgbd As String
            Get
                Return WsNomUtilisateurBd
            End Get
        End Property

        Public Property VirModeConnexion As String
            Get
                Return WsModeConnexion
            End Get
            Set(value As String)
                Select Case value
                    Case "AD", "V3", "V4", "VI"
                        WsModeConnexion = value
                    Case Else
                        WsModeConnexion = "V3V4"
                End Select
            End Set
        End Property

        ''****** AKR
        Public Property VirMdp() As String
            Get
                Return WsMdp
            End Get
            Set(value As String)
                Dim TabParam As List(Of String)
                If WslstMdp Is Nothing Then
                    WslstMdp = New List(Of VirSessionMdp)
                End If
                TabParam = Strings.Split(value, VI.PointVirgule, -1).ToList
                Dim virsession As VirSessionMdp
                virsession = New VirSessionMdp
                virsession.Session = TabParam(0)
                virsession.Nom = TabParam(1)
                virsession.Prenom = TabParam(2)
                virsession.Mdp = TabParam(3)
                WslstMdp.Add(virsession)
                WsMdp = value
            End Set
        End Property

        Public Property VirMdp(IdSession As String) As VirSessionMdp
            Get
                Return WslstMdp.Find(Function(Recherche) Recherche.Session = IdSession)
            End Get
            Set(value As VirSessionMdp)
            End Set
        End Property


        Public Structure VirSessionMdp
            Public Session As String
            Public Nom As String
            Public Prenom As String
            Public Mdp As String
        End Structure

        ''**** FIN AKR

        Public ReadOnly Property VirModele As Virtualia.Systeme.MetaModele.Donnees.ModeleRH
            Get
                Return WsRhModele
            End Get
        End Property

        Public ReadOnly Property VirRepertoire As String
            Get
                Return WsRepertoireVirtualia
            End Get
        End Property

        Public ReadOnly Property VirNoBd As Integer
            Get
                Return WsNoBd
            End Get
        End Property

        Public ReadOnly Property VirSgbd As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbd
            Get
                Return WsInstanceSgbd
            End Get
        End Property

        Public ReadOnly Property VirInstanceBd As Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbdDatabase
            Get
                Dim IndiceI As Integer

                For IndiceI = 0 To VirSgbd.NombredeDatabases - 1
                    Select Case VirSgbd.Item(IndiceI).Numero
                        Case Is = WsNoBd
                            Return VirSgbd.Item(IndiceI)
                    End Select
                Next IndiceI
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property VirRhDates As Virtualia.Systeme.Fonctions.CalculDates
            Get
                Return WsRhDates
            End Get
        End Property

        Public ReadOnly Property VirRhFonction As Virtualia.Systeme.Fonctions.Generales
            Get
                Return WsRhFct
            End Get
        End Property

        Public ReadOnly Property IntitulesMois As String
            Get
                Dim IndiceM As Integer
                Dim Chaine As New System.Text.StringBuilder
                For IndiceM = 1 To 12
                    Chaine.Append(WsRhDates.MoisEnClair(IndiceM) & VI.Tild)
                Next IndiceM
                Return Chaine.ToString
            End Get
        End Property

        Private Sub ChangerBaseCourante()
            Dim CodeRetour As Boolean
            Dim WcfServeur As Virtualia.Net.ServiceServeur.VirtualiaServeurClient

            WcfServeur = New Virtualia.Net.ServiceServeur.VirtualiaServeurClient
            CodeRetour = WcfServeur.ChangerlaBasedeDonnees(WsNomUtilisateurBd, WsNoBd)
            WcfServeur.Close()
        End Sub

        Public Sub EcrireLogTraitement(ByVal Nature As String, ByVal SiDatee As Boolean, ByVal Msg As String)
            Dim FicStream As System.IO.FileStream
            Dim FicWriter As System.IO.StreamWriter
            Dim NomLog As String
            Dim SysCodeIso As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)
            Dim dateValue As Date

            NomLog = Virtualia.Systeme.Constantes.DossierVirtualiaService("Logs") & Nature & ".log"
            FicStream = New System.IO.FileStream(NomLog, IO.FileMode.Append, IO.FileAccess.Write)
            FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)
            dateValue = System.DateTime.Now
            Select Case Msg
                Case Is = ""
                    FicWriter.WriteLine(Strings.StrDup(20, "-") & Strings.Space(1) & dateValue.ToString("dd/MM/yyyy hh:mm:ss.fff tt") & Space(1) & Strings.StrDup(20, "-"))
                Case Else
                    Select Case SiDatee
                        Case True
                            FicWriter.WriteLine(dateValue.ToString("dd/MM/yyyy hh:mm:ss.fff tt") & Space(1) & Msg)
                        Case False
                            FicWriter.WriteLine(Space(5) & Msg)
                    End Select
            End Select
            FicWriter.Flush()
            FicWriter.Close()
            FicStream.Close()
        End Sub

        Public Sub New(ByVal NomUti As String, ByVal NoBd As Integer)
            WsNomUtilisateurBd = NomUti
            WsNoBd = NoBd
            WsRhDates = New Virtualia.Systeme.Fonctions.CalculDates
            WsRhFct = New Virtualia.Systeme.Fonctions.Generales
            WsInstanceSgbd = New Virtualia.Systeme.Parametrage.BasesdeDonnees.ParamSgbd
            WsRhModele = New Virtualia.Systeme.MetaModele.Donnees.ModeleRH
            Call ChangerBaseCourante()
        End Sub
    End Class


End Namespace